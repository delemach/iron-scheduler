/*jslint nomen: true */
'use strict';
var Q = require('q');
var _ = require('underscore');
var config = require('./config');


var Scheduler = function (ironOauthToken, ironProjectId, logger) {

    if (!logger) {
        logger = config.logger;
    }

    var ironio = require('node-ironio')(ironOauthToken),
        project = ironio.projects(ironProjectId),
        scheduler = {};

    var queueTask = function (task, delay) {
        var deferred = Q.defer(),
            t = _.clone(task);
        if (delay) { t.delay = delay; }
        //if the payload of the task is filtered rename it here
        if (t.payloadSecret) { t.payload = t.payloadSecret; }

        //if the payload is itself JSON then stringify it
        if (!_.isString(t.payload) && _.isObject(t.payload)) {
            t.payload = JSON.stringify(t.payload);
        }

        project.tasks.queue(t, function (err, res) {
            if (err) {
                logger.error('Failed to queue a task! ' + err);
                deferred.reject(err);
            } else {
                logger.info('Successfully queued task! ID: %s', res.tasks[0].id);
                deferred.resolve(res);
            }
        });
        return deferred;
    };


    var processScheduledTasks = function (scheduledTasks) {
        var promises = [],
            now = new Date(),
            nowStr = now.toUTCString();

        logger.info('The time is now %s. Checking %d scheduled tasks to see what should run now...', nowStr, scheduledTasks.length);
        _.each(scheduledTasks, function (scheduledTask) {
            var regex = new RegExp(scheduledTask.schedule),
                number = Number(scheduledTask.number),
                interval = Number(scheduledTask.interval),
                i = 0,
                exp = null;

            //first check if any tasks are expired. For now just blow everything up if yes.
            if (scheduledTask.expires) {
                exp = new Date(scheduledTask.expires);
                if (now > exp) {
                    throw new Error('The scheduledTask named "' + scheduledTask.name + '" has expired on ' + scheduledTask.expires + '.');
                }
            }

            //If the interval is omitted or is explicitly set to zero then this will create a (one * (n-1)) second delay for each of the n tasks.
            //TODO: This will need to be fixed in order to support launching n tasks perfectly concurrently.
            if (!interval || interval < 1) { interval = 1; }

            logger.debug('Now: %s schedRegEx: %s', nowStr, scheduledTask.schedule);
            if (nowStr.match(regex)) {
                logger.info('Queueing %d scheduledTask(s) named "%s"...', number, scheduledTask.name);
                for (i = 0; i < (number * interval); i = i + interval) {
                    promises.push(queueTask(scheduledTask.task, i));
                }
            }
        });

        logger.info('Processed all matching scheduled tasks and found %d total tasks to queue...', promises.length);
        return Q.all(promises);

    };


    scheduler.run = function (tasks, cb) {
        processScheduledTasks(tasks)
            .catch(function (err) {
                if (!err) { err = 'Unknown error!'; }
                logger.error(err);
                if (cb) {
                    cb(err);
                }
            })
            .done(function () {
                if (cb) {
                    cb(null);
                }
            });
    };

    return scheduler;
};

module.exports = Scheduler;







