'use strict';
var winston = require('winston');
var config = {};



config.logInit = function (level) {
    level = level || 'info';
    var trnsprts = [
        new (winston.transports.Console)({ json: false, colorize: false, prettyPrint: true, level: level}),
        new (winston.transports.File)({ filename: 'iron-scheduler.log', level: level }) ];
    this.logger = new (winston.Logger)({ transports: trnsprts });
};
config.logInit();



module.exports = config;

