Iron Scheduler
========

## Overview ##

The Iron Scheduler is an [ironio worker](http://www.iron.io/worker) that queues configured tasks. It's the master in the master/slave setup that ironio [recommends](http://blog.iron.io/2011/06/worker-patterns-creating-task-level.html).

Iron Scheduler can be used as an independent worker, or you can use it programatically. 

## Usage as Worker ##

To use as a worker, [Create](http://dev.iron.io/worker/languages/nodejs/#upload_your_worker) Iron Scheduler as its own task in Iron.io. Then [schedule](http://dev.iron.io/worker/scheduling/) it to run as often as you like to check whether it should queue the tasks you configured.

## Configuration as Worker ##

Everything is configured in the payload that you give Iron Scheduler when you schedule it. The payload is JSON. Here's an example:

```javascript
{
    "ironOauthToken" : "YOUR_IRONIO_TOKEN",
    "ironProjectId" : "YOUR_IRONIO_PROJECT_ID",
    "scheduledTasks" : [
        {
            "name" : "My First Job",
            "schedule" : "\\b(?:13|17|21|01):.[012]:..\\b",
            "expires" : "Sun, 03 Nov 2013 09:00:00 GMT",
            "number" : "30",
            "interval" : "20",
            "task" : {
                "code_name" : "SomeTaskName",
                "payload" : {
                    "name0" : "value0",
                    "name1" : "value1"
                }
            }
        },
        {
            "name" : "My Second Job",
            "schedule" : "\\b..:(?:[03][012]|[14][567]):..\\b",
            "number" : "1",
            "interval" : "0",
            "task" : {
                "code_name" : "SomeOtherTaskName",
                "payloadSecret" : {
                    "name0" : "value0",
                    "name1" : "value1"
                }
            }
        }
    ]
}
```

Some explanation for the parameters in the scheduledTasks array:

1. **ironOauthToken** - This is the oauth token of the project _to which_ matching tasks will be queued. This can also be set as an environment variable named 'IRON\_TOKEN'. See the comments in the iron-scheduler.worker file. If passed in the payload it will override the environment variable.
1. **ironProjectId** - This is the project id of the project _to which_ matching tasks will be queued. This can also be set as an environment variable named 'IRON\_PROJECT\_ID'. See the comments in the iron-scheduler.worker file. If passed in the payload it will override the environment variable.
1. **name** - This value is used only by Iron Scheduler for logging out information on what it did to help you track what got queued. *Required.*
1. **schedule** - Whenever Iron Scheduler runs it uses this value to decide whether or not to queue the scheduled task. This value is a regular expression that is matched against the Date object UTC string. [This](http://www.debuggex.com/) page is extremely helpful in contructing regex strings that give you the matches you want. Don't forget to escape the backslashes when you put it into the JSON. *Required.*
1. **expires** - A date UTC string specifying until when Iron Scheduler should attempt to schedule this scheduled task. *Optional.*
1. **number** - When the schedule matches, this value specifies how many of the specified tasks to queue up. *Required.*
1. **interval** - When the schedule matches, this value specifies how long of an interval (in seconds) there should be between each of the tasks that will be queued. So for example, if **number** is 3 and **interval** is 20, Iron Schedule will queue three tasks, the first one with a delay of 0, the second with a delay of 20 and the third with a delay of 40. *Optional.*
1. **task** - Details of the actual task to queue. *Required.*

    1. **code_name** - The name of the task to queue. Must match the name that's in ironio. *Required.*
    1. **payload** OR **payloadSecret** - The payload to pass to the task that will be queued. Using **payloadSecret** will filter the entire payload from the HUD view showing runs of Iron Scheduler. It has no other effect. *Required.*

## Usage as Package ##

```
npm install iron-scheduler
```
Then in your own code:
```
var Scheduler = require('iron-scheduler');
var scheduler = Scheduler(ironOauthToken, ironProjectId, winstonLogger);
scheduler.run(scheduledTasks, function(err) {
    if (err) { 
        //handle error
    } else {
        //scheduler completed successfully. 
    }
});
```
The logger is a [winston](https://github.com/flatiron/winston) logger, and is optional. If omitted you'll get console and file logging through a winston console logger; see the config.js file for details.
The scheduledTasks parameter is an array of tasks in the same format as in the example payload above.

## Current Limitations / Future Work ##

* If a scheduled task is expired Iron Scheduler currently bombs (on purpose). Not sure what the correct logic should be.
* If **interval** is omitted or set to 0 then it is treated as 1. This means that if **number** is greater than 1, the queued tasks won't be scheduled to be perfectly concurrent; rather each will have a delay of (n-1) seconds.
* Right now each queued task is its own API call, but the ironio API supports queueing multiple tasks in one call.
* Currently no timezone support.

## License ##

MIT

