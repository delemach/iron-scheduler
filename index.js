/*jslint nomen: true */
'use strict';
require('JSON');
var fs = require('fs');
var _ = require('underscore');
var config = require('./config');
var Scheduler = require('./scheduler');


//load the payload values
process.argv.forEach(function (val, index, array) {
    var payload = '';
    if (val === '-payload') {
        payload = fs.readFileSync(process.argv[index + 1], 'ascii');
        _.extend(config, JSON.parse(payload));
    }
});

//if the payload did not set iron token and project values, look for environment variables
if (!config.ironOauthToken) { config.ironOauthToken = process.env.IRON_TOKEN; }
if (!config.ironProjectId) { config.ironProjectId = process.env.IRON_PROJECT_ID; }

var scheduler = Scheduler(config.ironOauthToken, config.ironProjectId, config.logger);
scheduler.run(config.scheduledTasks);





